package com.inimar.splashscreenforapp;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class FoodDetailsAdapter extends RecyclerView.Adapter<FoodDetailsAdapter.MyViewHolder> {

   List <DynamicRVModel> items;
    Activity activity;

    public FoodDetailsAdapter(RecyclerView recyclerView,Activity activity, List<DynamicRVModel> items) {
        this.activity = activity;
        this.items = items;

    }



        public static class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView ItemName,ItemDetails,FoodPrice;
        public Button add;
        public ImageView imageView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ItemName = itemView.findViewById(R.id.sel_item_name);
            ItemDetails = itemView.findViewById(R.id.sel_item_details);
            FoodPrice = itemView.findViewById(R.id.food_price);
            add = itemView.findViewById(R.id.add_food);
            imageView = itemView.findViewById(R.id.food_img);

        }


    }

    @NonNull
    @Override
    public FoodDetailsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.fragment_selected_items,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodDetailsAdapter.MyViewHolder holder, int position) {
        DynamicRVModel item = items.get(position);
        MyViewHolder myViewHolder= (MyViewHolder) holder;
        myViewHolder.ItemName.setText(item.getName());
        myViewHolder.ItemDetails.setText(item.getName());//Details Need to be added
        myViewHolder.FoodPrice.setText(item.getName());//Details Need to be added

        Drawable drawable = myViewHolder.itemView.getContext().getResources().getDrawable(item.getImage());
        myViewHolder.imageView.setImageDrawable(drawable);


    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
