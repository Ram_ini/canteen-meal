package com.inimar.splashscreenforapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class FoodDetailsFragment extends Fragment {

    List<DynamicRVModel> items = new ArrayList<>();
    FoodDetailsAdapter foodDetailsAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view =inflater.inflate(R.layout.fragment_food_details,container,false);


        items.add(new DynamicRVModel(R.drawable.khaja,"Khaja"));
        items.add(new DynamicRVModel(R.drawable.khana,"Khana"));
        items.add(new DynamicRVModel(R.drawable.drinks,"Drinks"));
        items.add(new DynamicRVModel(R.drawable.momos,"Momos"));
        items.add(new DynamicRVModel(R.drawable.dessert,"Dessert"));
        items.add(new DynamicRVModel(R.drawable.khaja,"Khaja"));
        items.add(new DynamicRVModel(R.drawable.khana,"Khana"));
        items.add(new DynamicRVModel(R.drawable.drinks,"Drinks"));
        items.add(new DynamicRVModel(R.drawable.momos,"Momos"));
        items.add(new DynamicRVModel(R.drawable.dessert,"Dessert"));


        RecyclerView rv = view.findViewById(R.id.rv_foodDetails);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        foodDetailsAdapter= new FoodDetailsAdapter(rv,getActivity(),items);
        rv.setAdapter(foodDetailsAdapter);

    return view;
    }
}
