package com.inimar.splashscreenforapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.jetbrains.annotations.NotNull;

public class SignUpTabFragment extends Fragment {
    EditText email,pass,phone,cPass;
    Button sgnUp;
    float v=0;
    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.signup_tab_fragment, container,false);


        email = root.findViewById(R.id.email);
        pass = root.findViewById(R.id.addr);
        phone = root.findViewById(R.id.phone);
        cPass = root.findViewById(R.id.cpass);
        sgnUp = root.findViewById(R.id.btn_sgnup);

        email.setTranslationX(800);
        phone.setTranslationX(800);
        pass.setTranslationX(800);
        cPass.setTranslationX(800);
        sgnUp.setTranslationX(800);

        email.setAlpha(v);
        phone.setAlpha(v);
        pass.setAlpha(v);
        cPass.setAlpha(v);
        sgnUp.setAlpha(v);

        email.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        phone.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        pass.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(700).start();
        cPass.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(900).start();
        sgnUp.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(1000).start();

        return root;
    }
}
